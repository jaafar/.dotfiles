#source ~/intel/mkl/bin/mklvars.sh intel64
#export PATH=/home/jaafar/elk-5.2.14/src:$PATH

export PATH=/usr/local/texlive/2019/bin/x86_64-linux:$PATH

export INFOPATH=$INFOPATH:/usr/local/texlive/2019/texmf-dist/doc/info

export MANPATH=$MANPATH:/usr/local/texlive/2019/texmf-dist/doc/man

export PATH=/home/jaafar/anaconda3/bin:$PATH

export VISUAL=vim

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

